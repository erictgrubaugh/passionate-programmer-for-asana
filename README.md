# README #

### What is this repository for? ###

If you've read [The Passionate Programmer](http://www.amazon.com/The-Passionate-Programmer-Remarkable-Development/dp/1934356344), by [Chad Fowler](https://twitter.com/chadfowler), then you know that, as advertised, it contains absolutely fantastic advice on building a remarkable career in software development.

If you haven't read it, and your job has anything to do with being a software developer or supervising them, then immediately stop everything you are doing, get your hands on the book, and absorb all of it.

I happen to use [Asana](https://asana.com/company) for all of my To-Do list needs. This repository is my attempt at building the Passionate Programmer tips and habits into a structured schedule on Asana.

### How do I get set up? ###

The repository contains multiple JSON files for importing projects and tasks into Asana. The projects are roughly equivalent to the major sections of the book, with tasks representing the individual chapters.

### Contribution guidelines ###

Contributions, ideas, and discussions are very welcome. Have suggestions? Feel free to create an Issue and describe your improvement. Better yet, create a pull request.

### Who do I talk to? ###

Want to start a conversation? You can contact me directly on twitter, [@erictgrubaugh](https://twitter.com/erictgrubaugh)